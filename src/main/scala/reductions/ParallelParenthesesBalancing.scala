package reductions

import scala.annotation._
import org.scalameter._
import common._

object ParallelParenthesesBalancingRunner {

  @volatile var seqResult = false

  @volatile var parResult = false

  val standardConfig = config(
    Key.exec.minWarmupRuns -> 40,
    Key.exec.maxWarmupRuns -> 80,
    Key.exec.benchRuns -> 120,
    Key.verbose -> true
  ) withWarmer(new Warmer.Default)

  def main2(args: Array[String]): Unit = {
    val length = 100000000
    val chars = new Array[Char](length)
    val threshold = 10000
    val seqtime = standardConfig measure {
      seqResult = ParallelParenthesesBalancing.balance(chars)
    }
    println(s"sequential result = $seqResult")
    println(s"sequential balancing time: $seqtime ms")

    val fjtime = standardConfig measure {
      parResult = ParallelParenthesesBalancing.parBalance(chars, threshold)
    }
    println(s"parallel result = $parResult")
    println(s"parallel balancing time: $fjtime ms")
    println(s"speedup: ${seqtime / fjtime}")
  }

  def main(args: Array[String]): Unit = {
    val chars = Array('(',')','(',')','(',')','(',')')
    val threshold = 1
    val result = ParallelParenthesesBalancing.parBalance(chars, threshold)
    println("Result = " + result)
  }
}

object ParallelParenthesesBalancing {

  /** Returns `true` iff the parentheses in the input `chars` are balanced.
   */
  def balance(chars: Array[Char]): Boolean = {
    var i = 0
    var n = 0
    while(n >= 0 && i < chars.length) {
      val c = chars(i)
      n += (if (c == '(') 1 else if (c == ')') -1 else 0)
      i += 1
    }
    n == 0
  }

  /** Returns `true` iff the parentheses in the input `chars` are balanced.
   */
  def parBalance(chars: Array[Char], threshold: Int): Boolean = {

    def traverse(idx: Int, until: Int, arg1: Int, arg2: Int): (Int, Int) = {
      if (idx == until) (arg1, arg2)
      else {
        val c = chars(idx)
        if (c == ')') {
          val _arg2 = arg2 - 1
          val _arg1 = if (_arg2 < 0) arg1 - 1 else arg1
          traverse(idx + 1, until, _arg1, _arg2 max 0)
        }
        else if (c == '(') traverse(idx + 1, until, arg1, arg2 + 1)
        else traverse(idx + 1, until, arg1, arg2)
      }
    }

    def reduce(from: Int, until: Int): (Int, Int) = {
      if ((until - from) / 2 <  threshold) traverse(from, until, 0, 0)
      else {
        val m = from + (until - from) / 2
        val (l,r) = parallel(reduce(from, m), reduce(m, until))
        val x = l._2 + r._1
        val left = l._1
        val right = r._2
        if (x > 0) (left, right + x) else (left + x, right)
      }
    }

    reduce(0, chars.length) == (0, 0)
  }

  // For those who want more:
  // Prove that your reduction operator is associative!

}
